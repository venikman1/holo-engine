#pragma once

#include "terminal.hpp"

#include <string>
#include <iostream>
#include <ncurses.h>

namespace holo4ka {
    class MessageBox : public Window
    {
    public:
        MessageBox(const std::string& message, int ypadding = 0, int xpadding = 0)
        : Window (3 + ypadding * 2, message.size() + 2 + xpadding * 2, (LINES - 3 - ypadding * 2) / 2, (COLS - message.size() - 2 - xpadding * 2) / 2),
            message(message),
            ypadding(ypadding),
            xpadding(xpadding)
        {}

        void draw() override {
            Window::draw();
            mvwprintw(win, 1 + ypadding, 1 + xpadding, message.c_str());
        }

        void touch() override {
            std::cerr << "Window is touched" << std::endl;
            Window::touch();
        }

    private:
        std::string message;
        int ypadding, xpadding;
    };
}
