#pragma once

#include <ncurses.h>
#include <locale.h>

#include <memory>
#include <map>
#include <vector>

namespace holo4ka {
    class WindowInterface {
    public:
        virtual void noutrefresh() = 0;
        virtual void refresh() = 0;
        virtual void touch() = 0;
        virtual void force_refresh() = 0;
        virtual void draw() = 0;
        virtual WINDOW* get_window() = 0;

        virtual ~WindowInterface() = default;
    };

    class Window : public WindowInterface {
    public:
        Window(int height, int width, int starty, int startx) :
        height(height),
        width(width),
        starty(starty),
        startx(startx) 
        {
            win = newwin(height, width, starty, startx);
        }

        void refresh() override {
            wrefresh(win);
        }

        void noutrefresh() override {
            wnoutrefresh(win);
        }

        void touch() override {
            touchwin(win);
        }

        void force_refresh() override {
            this->touch();
            this->refresh();
        }

        void draw() override {
            box(win, 0, 0);
        }

        WINDOW* get_window() override {
            return win;
        }

        ~Window() {
            delwin(win);
        }

    protected:
        WINDOW* win;
        int height, width, starty, startx;
    };

    class Terminal {
    public:
        using WindowId = size_t;

        Terminal() = default;

        void init() const {
            setlocale(LC_ALL, "C.UTF-8"/*"en_US.UTF-8"*/);
            initscr();
            cbreak();
            noecho();
            //timeout(0);
            curs_set(0);
        }

        void stop() const {
            endwin();
        }

        void push_window(WindowInterface* window) {
            window_stack.push_back(std::unique_ptr<WindowInterface>(window));
        }

        void pop_window() {
            if (!window_stack.empty())
                window_stack.pop_back();
        }

        void refresh_all_windows() {
            touchwin(stdscr);
            wnoutrefresh(stdscr);
            for (auto& window : window_stack) {
                window->touch();
                window->noutrefresh();
            }
            doupdate();
        }

    private:
        std::vector<std::unique_ptr<WindowInterface>> window_stack;
    };
}
