#pragma once

#include "terminal.hpp"

namespace holo4ka {
    class ViewportInterface {
    public:
        virtual const char* get_block(int y, int x) const = 0;
    };

    class ViewportWindow : public Window {
    public:
        ViewportWindow(int height, int width, int starty, int startx)
        : Window(height, width, starty, startx)
        {}

        void draw() override {
            Window::draw();
            if (viewport) {
                for (int i = 0; i < height - 2; ++i) {
                    for (int j = 0; j < width - 2; ++j) {
                        mvwaddstr(win, i + 1, j + 1, viewport->get_block(i, j));
                    }
                }
            }
        }

        const ViewportInterface* viewport = nullptr;
    };
}