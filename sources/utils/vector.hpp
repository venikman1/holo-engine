#pragma once

#include <array>

namespace holo4ka {
    template <class T, int LENGTH>
    class Vector : public std::array<T, LENGTH> {
    public:
        Vector operator+() const {
            return *this;
        }

        Vector operator-() const {
            Vector result;
            for (size_t i = 0; i < result.size(); ++i) {
                result[i] = -(*this)[i];
            }
            return result;
        }

        Vector& operator+=(const Vector& other) {
            for (size_t i = 0; i < other.size(); ++i) {
                (*this)[i] += other[i];
            }
            return *this;
        }

        Vector& operator-=(const Vector& other) {
            for (size_t i = 0; i < other.size(); ++i) {
                (*this)[i] -= other[i];
            }
            return *this;
        }

        Vector& operator*=(const T& other) {
            for (size_t i = 0; i < this->size(); ++i) {
                (*this)[i] *= other;
            }
            return *this;
        }

        Vector& operator/=(const T& other) {
            for (size_t i = 0; i < this->size(); ++i) {
                (*this)[i] /= other;
            }
            return *this;
        }

        friend Vector operator+(const Vector& v1, const Vector& v2) {
            Vector result = v1;
            result += v2;
            return result;
        }

        friend Vector operator-(const Vector& v1, const Vector& v2) {
            Vector result = v1;
            result -= v2;
            return result;
        }

        friend Vector operator*(const Vector& v1, const T& t) {
            Vector result = v1;
            result *= t;
            return result;
        }

        friend Vector operator/(const Vector& v1, const T& t) {
            Vector result = v1;
            result /= t;
            return result;
        }

        template <int L = LENGTH>
        std::enable_if_t<(LENGTH >= 1), T&> get_x() {
            return (*this)[0];
        }

        template <int L = LENGTH>
        std::enable_if_t<(LENGTH >= 2), T&> get_y() {
            return (*this)[1];
        }

        template <int L = LENGTH>
        std::enable_if_t<(L >= 3), T&> get_z() {
            return (*this)[2];
        }
    };

    using Vector2i = Vector<int, 2>;
}
