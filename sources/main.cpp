#include <cursesw.h>

#include <string>
#include <iostream>

#include "engine/terminal/terminal.hpp"
#include "engine/terminal/custom_windows.hpp"
#include "viewport.hpp"
#include "./utils/vector.hpp"

void window_test() {
    std::string text = "Holo \xe2\x96\x88 the best wolf awoooooooooooooooooooooooooooooooooooooooooooooo";
    
    int height = 30;
    int width = 120;
    int starty = 3;
    int startx = 3;
    holo4ka::Window window(height, width, starty, startx);
    window.draw();
    mvwaddstr(window.get_window(), 1, 1, "H0lo4ka is truly the best █");
    mvaddstr(1, 0, "assdwwq ██████████████████████████████████");
    
    window.refresh();
    getch();
    holo4ka::WindowInterface *box = new holo4ka::MessageBox(text, 3, 3);
    box->draw();
    box->refresh();
    getch();
    window.force_refresh();
    getch();
    box->force_refresh();
    getch();
    delete(box);
    getch();
    window.force_refresh();
    getch();
}

void HUD_init() {
    int height = 4;
    int width = 7;
    int starty = 51;
    int startx = 1;
    holo4ka::WindowInterface *inv_win_0 = new holo4ka::Window(height, width, starty, startx);
    inv_win_0->draw();
    mvwaddstr(inv_win_0->get_window(), 3, 6, "1");
    inv_win_0->refresh();
    holo4ka::WindowInterface *inv_win_1 = new holo4ka::Window(height, width, starty, startx + 10);
    inv_win_1->draw();
    mvwaddstr(inv_win_1->get_window(), 3, 6, "2");
    inv_win_1->refresh();
    holo4ka::WindowInterface *inv_win_2 = new holo4ka::Window(height, width, starty, startx + 20);
    inv_win_2->draw();
    mvwaddstr(inv_win_2->get_window(), 3, 6, "3");
    inv_win_2->refresh();
    holo4ka::WindowInterface *inv_win_3 = new holo4ka::Window(height, width, starty, startx + 30);
    inv_win_3->draw();
    mvwaddstr(inv_win_3->get_window(), 3, 6, "4");
    inv_win_3->refresh();
    holo4ka::WindowInterface *inv_win_4 = new holo4ka::Window(height, width, starty, startx + 40);
    inv_win_4->draw();
    mvwaddstr(inv_win_4->get_window(), 3, 6, "5");
    inv_win_4->refresh();
    holo4ka::WindowInterface *inv_win_5 = new holo4ka::Window(height, width, starty, startx + 50);
    inv_win_5->draw();
    mvwaddstr(inv_win_5->get_window(), 3, 6, "6");
    inv_win_5->refresh();
    holo4ka::WindowInterface *inv_win_6 = new holo4ka::Window(height, width, starty, startx + 60);
    inv_win_6->draw();
    mvwaddstr(inv_win_6->get_window(), 3, 6, "7");
    inv_win_6->refresh();
    holo4ka::WindowInterface *inv_win_7 = new holo4ka::Window(height, width, starty, startx + 70);
    inv_win_7->draw();
    mvwaddstr(inv_win_7->get_window(), 3, 6, "8");
    inv_win_7->refresh();
    holo4ka::WindowInterface *inv_win_8 = new holo4ka::Window(height, width, starty, startx + 80);
    inv_win_8->draw();
    mvwaddstr(inv_win_8->get_window(), 3, 6, "9");
    inv_win_8->refresh();
    holo4ka::WindowInterface *inv_win_9 = new holo4ka::Window(height, width, starty, startx + 90);
    inv_win_9->draw();
    mvwaddstr(inv_win_9->get_window(), 3, 6, "0");
    inv_win_9->refresh();
    holo4ka::WindowInterface *status_win = new holo4ka::Window(30, 50, 1, 153);
    status_win->draw();
    mvwaddstr(status_win->get_window(), 3, 6, "Type q to exit, wasd to move");
    status_win->refresh();
}

void viewport_test() {
    holo4ka::Viewport viewport("maps/test_map.txt");
    //holo4ka::World world;
    viewport.move_player(25, 75);

    int height = 50;
    int width = 150;
    int starty = 1;
    int startx = 1;
    holo4ka::ViewportWindow window(height, width, starty, startx);
    window.viewport = &viewport;
    window.draw();
    window.refresh();
    keypad(stdscr, TRUE);
    int input;
    while ((input = getch()) != 'q') {
        switch (input) {
        case KEY_UP:
            viewport.move_player_relative_if_blank(-1, 0);
            break;
        case KEY_LEFT:
            viewport.move_player_relative_if_blank(0, -1);
            break;
        case KEY_DOWN:
            viewport.move_player_relative_if_blank(1, 0);
            break;
        case KEY_RIGHT:
            viewport.move_player_relative_if_blank(0, 1);
            break;
        }
        window.draw();
        window.refresh();
    }
}

int main() {
    holo4ka::Terminal terminal;

    terminal.init();
    mvprintw(0, 0, "Term size is %d x %d", LINES, COLS);
    mvprintw(1, 0, "Press q to quit, w to close windows, e to open");
    refresh();

    holo4ka::Window* win;
    win = new holo4ka::Window(3, 40, 3, 3);
    mvwprintw(win->get_window(), 1, 1, "█Holo is the best waifu %d times█", 1);
    win->draw();
    int i = 1;
    terminal.push_window(win);
    terminal.refresh_all_windows();

    int c;
    while ((c = getch()) != 'q') {
        if (c == 'e') {
            win = new holo4ka::Window(3, 40, 3 + i * 2, 3 + i * 2);
            mvwprintw(win->get_window(), 1, 1, "█Holo is the best waifu %d times█", ++i);
            win->draw();
            terminal.push_window(win);
        }
        if (c == 'w') {
            terminal.pop_window();
            if (i > 0)
                --i;
        }
        terminal.refresh_all_windows();
    }
    
    while (i--)
        terminal.pop_window();
    terminal.stop();
    
    return 0;
}
