#pragma once

#include <vector>
#include <string>
#include <utility>

#include "engine/terminal/game_windows.hpp"
#include "utils/file.hpp"

namespace holo4ka {
    class Viewport : public ViewportInterface {
    public:
        Viewport(std::vector<std::vector<int>> map) : map(std::move(map)) {}
        Viewport(const std::string& filename) {
            std::vector<char> file_data = read_file(filename);
            map.emplace_back();
            for (char c : file_data) {
                if (c == '\n') {
                    map.emplace_back();
                } else {
                    map.back().push_back(c);
                }
            }
        }

        const char* get_block(int y, int x) const override {
            if (x == playerx && y == playery)
                return player_symbol;
            if (y < 0 || y >= (int)map.size())
                return blank_symbol;
            const std::vector<int>& row = map[y];
            if (x < 0 || x >= (int)row.size())
                return blank_symbol;
            return (row[x] == '#' ? wall_symbol : blank_symbol);
        }

        void move_player(int newy, int newx) {
            playery = newy;
            playerx = newx;
        }

        void move_player_if_blank(int newy, int newx) {
            if (get_block(newy, newx) == blank_symbol)
                move_player(newy, newx);
        }

        void move_player_relative(int rely, int relx) {
            playery += rely;
            playerx += relx;
        }

        void move_player_relative_if_blank(int rely, int relx) {
            if (get_block(playery + rely, playerx + relx) == blank_symbol)
                move_player_relative(rely, relx);
        }

    private:        
        int playerx, playery;
        std::vector<std::vector<int>> map;
        static const char* blank_symbol, *wall_symbol, *player_symbol;
    };

    const char* Viewport::blank_symbol = " ";
    const char* Viewport::wall_symbol = "#";
    const char* Viewport::player_symbol = "@";
}
