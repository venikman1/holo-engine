# Build game
FROM gcc:latest as build

RUN apt-get update -qq && apt-get install -y -qq libncurses5-dev libncursesw5-dev cmake

ADD . /app/holo-engine/

WORKDIR /app/build/

RUN cmake ../holo-engine/
RUN cmake --build .

# Launch game
FROM ubuntu:latest

WORKDIR /app

COPY --from=build /app/build/holo4ka .
COPY --from=build /app/holo-engine/maps ./maps/

ADD web-term /app/web-term

# Settings for tzdata
ENV TZ=Europe/Moscow
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt-get update -qq && apt-get install -y -qq nginx python3 python3-pip
RUN pip3 install -r /app/web-term/requirements.txt

RUN cp /app/web-term/nginx.conf /etc/nginx/sites-available/default

# RUN groupadd -r player && useradd -r -g player player
# USER player

ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8
ENV TERM=linux

ENTRYPOINT ["/bin/bash", "/app/web-term/run_in_docker.sh"]
