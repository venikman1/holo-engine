#!/usr/bin/env python

import asyncio
import websockets
import sys

import process_communication


async def echo(websocket, path):
    async for message in websocket:
        print("Recieved: {}".format(repr(message)))
    # await process_communication.emulate_running(["/home/venikman1/coding/holo-engine/build/holo4ka"], websocket)
    # await process_communication.emulate_running(["nano", "/home/venikman1/coding/holo-engine/web-term/process_communication.py"], websocket)
    # while True:
    #     await websocket.send("Rjaka\r\n")
    #     await asyncio.sleep(2.0)
        # await websocket.send(message)


async def consumer_handler(websocket, proc, master_fd):
    async for message in websocket:
        print("Recieved: {}".format(repr(message)))
        await asyncio.get_event_loop().run_in_executor(
            None,
            process_communication.write_to_fd,
            master_fd,
            message,
        )


async def producer_handler(websocket, proc, master_fd):
    while proc.poll() is None:
        loop = asyncio.get_event_loop()
        future = asyncio.Future()

        def read_data_into_future():
            data = process_communication.read_from_fd(master_fd)
            future.set_result(data)

        loop.add_reader(master_fd, read_data_into_future)
        future.add_done_callback(lambda f: loop.remove_reader(master_fd))
        await future

        data = future.result()
        if data is not None:
            print("Send: {}".format(repr(data)))
            await websocket.send(data)
        # data = await asyncio.get_event_loop().run_in_executor(
        #     None,
        #     process_communication.read_from_fd,
        #     master_fd,
        # )
        # if data is not None:
        #     print("Send: {}".format(repr(data)))
        #     await websocket.send(data)


async def handler(websocket, path):
    assert len(sys.argv) == 3
    binary_path = sys.argv[1]
    cwd = sys.argv[2]
    proc, master_fd, slave_fd = process_communication.emulate_running(
        [binary_path],
        cwd=cwd,
    )
    print("Started process with pty as master={}".format(master_fd))
    consumer_task = asyncio.ensure_future(consumer_handler(websocket, proc, master_fd))
    producer_task = asyncio.ensure_future(producer_handler(websocket, proc, master_fd))
    done, pending = await asyncio.wait(
        [consumer_task, producer_task],
        return_when=asyncio.FIRST_COMPLETED,
    )
    for task in pending:
        task.cancel()
    process_communication.close_pty(master_fd, slave_fd)
    print("Closed tty")
    proc.kill()
    print("Closed process")

start_server = websockets.serve(handler, "", 8765)

asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()
