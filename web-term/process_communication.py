import subprocess
from typing import List
import sys
import threading
import os
import pty
import tty
import signal
import select
import termios
import struct
import fcntl


def set_winsize(fd, row, col, xpix=0, ypix=0):
    winsize = struct.pack("HHHH", row, col, xpix, ypix)
    fcntl.ioctl(fd, termios.TIOCSWINSZ, winsize)


def emulate_running(exec: List[str], cwd="/home/venikman1/coding/holo-engine/"):
    """
    Emulate working of process in tty
    :param exec: exec command
    :param cwd: working directory
    :return: (Popen object, pty master fd, pty slave fd)
    """
    master, slave = pty.openpty()
    set_winsize(master, 50, 230)
    tty.setraw(slave)


    default = [9474, 5, 191, 35387, 15, 15, [b'\x03', b'\x1c', b'\x7f', b'\x15', b'\x04', b'\x00', b'\x01', b'\x00', b'\x11', b'\x13', b'\x1a', b'\x00', b'\x12', b'\x0f', b'\x17', b'\x16', b'\x00', b'\x00', b'\x00', b'\x00', b'\x00', b'\x00', b'\x00', b'\x00', b'\x00', b'\x00', b'\x00', b'\x00', b'\x00', b'\x00', b'\x00', b'\x00']]
    termios.tcsetattr(slave, termios.TCSANOW, default)

    proc = subprocess.Popen(
        exec,
        shell=False,
        stdin=slave,
        stdout=slave,
        stderr=subprocess.DEVNULL,
        cwd=cwd,
    )
    return proc, master, slave


def read_from_fd(fd, size=1024):
    rlist, _, _ = select.select([fd], [], [], 2.0)
    for f in rlist:
        return os.read(rlist[0], size)
    return None


def write_to_fd(fd, data):
    os.write(fd, data)


def close_pty(master, slave):
    os.close(master)
    os.close(slave)
