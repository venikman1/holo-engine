#!/bin/bash
./docker-build.sh

error=false

CID=$(docker run -d -p 23323:80 -p 23324:8765 --rm --name holo-container holo-image)

if [[ $CID =~ ^[0-9A-Fa-f]{1,}$ ]] ; then
    echo "Container $CID is started"
else
    echo "Container cannot be started for some reason"
    exit 1
fi

echo "Sleeping for 5 seconds..."
sleep 5

site_status=$(curl -o -I -L -s -w "%{http_code}\n" http://localhost:23323)
if [ $site_status != "200" ]; then
    echo "Wrong HTTP status on website. Got $site_status, expected 200"
    echo "(probably nginx isn't working properly)"
    error=true
else
    echo "Website response status is $site_status"
fi

site_status=$(curl -o -I -L -s -w "%{http_code}\n" http://localhost:23324)
if [ $site_status != "426" ]; then
    echo "Wrong HTTP status on WS server. Got $site_status, expected 426"
    echo "(probably ws_server isnt working properly)"
    error=true
else
    echo "WS server status is $site_status"
fi

docker stop holo-container
echo "Container has been stopped"

if [ $error == "true" ]; then
    echo "There was some errors, test is failed"
    exit 1;
else
    echo "Everything is fine"
fi
